<?php

declare(strict_types=1);

namespace Anilken\PayTr\Exceptions;

use Exception;

class InvalidOrderDataException extends Exception
{
}
